<?php

return [
    'Signup' => 'Регистрация',
    'Login' => 'Вход',
    'My profile' => 'Моя страница',
    'Create post' => 'Создать Публикацию',
    'Newsfeed' => 'Новостная лента',
    'Logout ({username})' => 'Выход ({username})',
];